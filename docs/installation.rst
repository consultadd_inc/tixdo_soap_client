.. highlight:: shell

============
Installation
============

At the command line::

    $ easy_install tixdo_soap_client

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv tixdo_soap_client
    $ pip install tixdo_soap_client
