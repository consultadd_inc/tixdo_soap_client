
certificates = {
    "U/A": "ua",
    "U": "u",
    "A": "a",
    "S": "s",
    "": None
}

REMOTE = {
    'remote': 'inox'
}

TIXDO_MOVIE = {
    'sub_movies': 'sub_movies',
}

LANGUAGE = {
    'default': 'hindi',
}

DIMENSION = {
    'default': '2D',
}

INOX_MOVIE = {
    'title': 'movieName',
    'id': 'movieId',
    'duration': 'movieDuration',
    'release_date': 'movieReleaseDate',
    'certificate': 'movieCertification',
    'genre': 'movieGenre',
    'dimension': 'format',
    'poster': 'movieImage',
    'description': 'synopsis',
    'cast': 'movieCast',
    'directors': 'movieDirectors',
}

INOX_THEATRE = {
    'code': 'Cinema_strID',
    'open': 'open',
    'close': 'close',
}

INOX_SHOW = {
    'code': 'Session_lngSessionId',
    'price_group_code': 'PGroup_strCode',
}
