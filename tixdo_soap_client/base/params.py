from .settings import INOX_Af_usr
from .settings import INOX_Af_ps


GET_ACTIVE_MOVIES = {"Af_usr": INOX_Af_usr,
                     "Af_ps": INOX_Af_ps,
                     "CityID": "",
                     "MovieID": 0}

GET_CINEMA_LIST = {"Af_usr": INOX_Af_usr,
                   "Af_ps": INOX_Af_ps,
                   "latitude": "",
                   "longitude": "",
                   "cityID": "",
                   "Date": "",
                   "CinemaID": 0}

GET_CITY_LIST = {"Af_usr": INOX_Af_usr,
                 "Af_ps": INOX_Af_ps,
                 "searchText": ""}

GET_SCHEDULE_CINEMA_MOVIE_TIMINGS = {"Af_usr": INOX_Af_usr,
                                     "Af_ps": INOX_Af_ps,
                                     "cityID": "",
                                     "Date": "",
                                     "CinemaID": "",
                                     "UEm": "",
                                     "UMob": ""}

GET_SCHEDULE_DATES = {"Af_usr": INOX_Af_usr,
                      "Af_ps": INOX_Af_ps,
                      "CityID": ""}

MOVIE_DETAILS = {"Af_usr": INOX_Af_usr,
                 "Af_ps": INOX_Af_ps,
                 "CityID": 0,
                 "MovieID": ""}

SCHEDULE_MOVIE_THEATRES = {"Af_usr": INOX_Af_usr,
                           "Af_ps": INOX_Af_ps,
                           "cityID": "",
                           "showDate": "",
                           "MovieID": "",
                           "UEm": "",
                           "UMob": ""}

TRANSACTION_DETAILS = {"Af_usr": INOX_Af_usr,
                       "Af_ps": INOX_Af_ps,
                       "startDate": "",
                       "endDate": "",
                       "UEm": "",
                       "UMob": ""}

GET_SCHEDULE_MOVIES_LIST = {"Af_usr": INOX_Af_usr,
                            "Af_ps": INOX_Af_ps,
                            "cityID": "",
                            "showDate": ""}