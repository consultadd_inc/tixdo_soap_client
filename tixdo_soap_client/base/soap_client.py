import requests
import json


class Client:
    def __init__(self, url):
        self.url = url
        self.data = {}
        self.headers = {'content-type': 'text/xml', 'charset': 'utf-8'}
        self.body = """<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            {0}
                            {1}
                            {2}
                          </soap:Body>
                        </soap:Envelope>"""

    def join(self, x):
        self.url.join([x])

    def generate_params(self, data):
        body = []
        for k, v in data.items():
            body.append(''.join(['<', str(k), '>', str(v), '</', str(k), '>']))
        return ''.join(body)

    def generate_body(self, data, endpoint):
        self.data = data
        service_start = '<' + endpoint + ' xmlns="http://tempuri.org/">'
        service_end = '</' + endpoint + '>'
        self.body = self.body.format(service_start, self.generate_params(self.data), service_end)

    def post(self, data, endpoint):
        self.endpoint = endpoint
        self.generate_body(data, endpoint)
        response = requests.post(self.url, data=self.body, headers=self.headers)
        return response

    @staticmethod
    def client_response(url, data, service):
        client = Client(url)
        response = client.post(data, service)

        # Converting Byte response to String object
        decoded = response.content.decode('utf-8', errors='ignore')

        # Converting String object into JSON object
        decoded_dict = json.loads(decoded)

        return decoded_dict