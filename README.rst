===============================
tixdo_soap_client
===============================

.. image:: https://img.shields.io/pypi/v/tixdo_soap_client.svg
        :target: https://pypi.python.org/pypi/tixdo_soap_client

.. image:: https://img.shields.io/travis/ayushj31/tixdo_soap_client.svg
        :target: https://travis-ci.org/ayushj31/tixdo_soap_client

.. image:: https://readthedocs.org/projects/tixdo_soap_client/badge/?version=latest
        :target: https://readthedocs.org/projects/tixdo_soap_client/?badge=latest
        :alt: Documentation Status


Python Boilerplate contains all the boilerplate you need to create a Python package.

* Free software: ISC license
* Documentation: https://tixdo_soap_client.readthedocs.org.

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
