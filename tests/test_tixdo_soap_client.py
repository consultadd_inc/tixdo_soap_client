#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_tixdo_soap_client
----------------------------------

Tests for `tixdo_soap_client` module.
"""

import unittest

from tixdo_soap_client import tixdo_soap_client


class TestTixdo_soap_client(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_000_something(self):
        pass


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
